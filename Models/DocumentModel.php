<?php

    class DocumentModel {
        public $id;
        public $leaseId;
        public $type;
        public $file;

        public function outputJsonInfo(): string
        {
            return '{id: int, leaseId: int, type: int}';
        }

        public function outputJson(): string
        {
            return '{id: int, leaseId: int, type: int, file: blob}';
        }
    }

?>