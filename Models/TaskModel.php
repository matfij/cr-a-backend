<?php

    class TaskModel {
        public $id;
        public $leaseId;
        public $userId;
        public $type;
        public $status;
        public $address;

        public function outputJson(): string
        {
            return '{leaseId: int, userId: int, type: int, status: int, address: string}';
        }
    }

?>