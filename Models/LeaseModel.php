<?php

    class LeaseModel {
        public $id;
        public $clientId;
        public $carId;
        public $startDate;
        public $endDate;
        public $status;
        public $department;

        public function outputJson(): string
        {
            return '{clientId: int, carId: int, startDate: string, endDate: string, status: int, department: string}';
        }
    }

?>