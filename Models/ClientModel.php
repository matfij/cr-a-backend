<?php

    class ClientModel {
        public $id;
        public $name;
        public $surname;
        public $age;
        public $pesel;
        public $address;
        public $email;
        public $phone;

        public function outputJson(): string
        {
            return '{name: string, surname: string, age: int, pesel: string, address: string, email: string, phone: int}';
        }
    }

?>