<?php

    class UserModel {
        public $id;
        public $login;
        public $password;
        public $email;
        public $role;
        public $department;

        public function outputJson(): string
        {
            return '{login: string, password: string, email: string, role: int, department: string}';
        }
    }

?>