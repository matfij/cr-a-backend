<?php

    class CarModel {
        public $id;
        public $department;
        public $registrationNumber;
        public $brand;
        public $model;
        public $acriss;
        public $tires;
        public $vin;
        public $productionYear;
        public $status;

        public function outputJson(): string
        {
            return '{department: string, registrationNumber: string, brand: string, model: string, acriss: string, tires: string, vin: string, productionYear: int, status: int}';
        }
        
    }

?>