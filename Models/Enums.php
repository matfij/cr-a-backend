<?php

    class Enums {

        private $userRole = 'userRole <br>
        { <br> 
            &nbsp; 0: "administrator", <br> 
            &nbsp; 1: "director", <br> 
            &nbsp; 2: "office", <br> 
            &nbsp; 3: "driver", <br> 
            &nbsp; 4: "broker" <br> 
        }';

        private $carStatus = 'carStatus <br>
        { <br> 
            &nbsp; 0: "in preparation", <br> 
            &nbsp; 1: "in exploatation", <br> 
            &nbsp; 2: "in in use", <br> 
            &nbsp; 3: "in maintenance", <br> 
            &nbsp; 4: "withdrawn" <br> 
        }';

        private $leaseStatus = 'leaseStatus <br>
        { <br> 
            &nbsp; 0: "open", <br> 
            &nbsp; 1: "started", <br> 
            &nbsp; 2: "finished", <br> 
            &nbsp; 3: "cancelled" <br> 
        }';

        private $taskType = 'taskType <br>
        {  <br> 
            &nbsp; 0: "hand car", <br> 
            &nbsp; 1: "pick car", <br> 
            &nbsp; 2: "inspection", <br> 
            &nbsp; 3: "relocation" <br> 
        }';

        private $taskStatus = 'taskStatus <br>
        { <br> 
            &nbsp; 0: "open", <br> 
            &nbsp; 1: "finished", <br> 
            &nbsp; 2: "cancelled" <br>
        }';

        public function outputJson(): string
        {
            return $this->userRole.'<br><br>'.$this->carStatus.'<br><br>'.$this->leaseStatus.'<br><br>'.$this->taskType.'<br><br>'.$this->taskStatus;
        }
    }

?>