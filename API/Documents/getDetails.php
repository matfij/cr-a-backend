<?php

    // headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: GET');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Access-Control-Allow-Methods, Content-Type, Authorization, X-Requested-With');

    include_once '../../Config/Database.php';
    include_once '../../Models/DocumentModel.php';
    include_once '../../Controllers/DocumentController.php';

    // headers routing
    foreach (getallheaders() as $name => $value) {
        if ($name == 'swagger' && $value = 'swagger-ngx') {
            $document = new DocumentModel();
            echo '"<b>Input: {id: int}</b><br/><b>Output: </b>'.$document->outputJson().'</b><br/>"';
            exit();
        }
    }

    // db connection
    $database = new Database();
    $connection = $database->connect();

    // getting payload
    $data = json_decode(file_get_contents("php://input"));
    $id = $data->id;

    $controller = new DocumentController($connection);

    // result
    $result = $controller->getById($id);
    
    if ($result != null) {
        echo $result["file"];
    } else {
        echo json_encode(
            array('message' => 'Unable to fetch data.')
        );
    }

?>