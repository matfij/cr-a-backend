<?php

    // headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: GET');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Access-Control-Allow-Methods, Content-Type, Authorization, X-Requested-With');
    error_reporting(null);

    include_once '../../Config/Database.php';
    include_once '../../Controllers/DocumentController.php';
    require_once 'TCPDF/tcpdf.php';

    // headers routing
    foreach (getallheaders() as $name => $value) {
        if ($name == 'swagger' && $value = 'swagger-ngx') {
            echo '"<b>Input: {leaseId: int, comments: string}</b>{name: string, pesel: int}<br/><b>Output: </b> {pdfFile: blob} </b><br/>"';
            exit();
        }
    }

    // db connection
    $database = new Database();
    $connection = $database->connect();

    // getting payload
    $data = json_decode(file_get_contents("php://input"));
    
    // content generation
    $controller = new DocumentController($connection);
    $html = $controller->generateLetOutProtocol($data->$leaseId, $data->$comments);

    // pdf formation
    $pdf = new TCPDF('p', 'mm', 'A4');
    $pdf->AddPage();
    $pdf->writeHTMLCell(190, 0, '', '', $html);

    // result
    $pdf->Output();

?>