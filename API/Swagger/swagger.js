

function expand(directory, endpoint) {
    const desc = document.getElementById("endpointDescription"+directory+endpoint);
    const url='http://mts.wibro.agh.edu.pl/~s292511/php/API/'+directory+'/'+endpoint;

    $.ajax({
        url: url,
        headers: {
            'swagger': 'swagger-ngx'
        },
        type: 'POST',
        success: function(result) {
            if (desc.innerHTML == "") {
                desc.innerHTML = result;
            } else {
                desc.innerHTML = "";
            }
        },
        error: function(error) {
        }
    })
}