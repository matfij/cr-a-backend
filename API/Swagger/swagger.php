
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>CarRent API</title>

    <link rel="stylesheet" href="swagger.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  </head>
  
  <body>

    <div class="title">
      <b>CarRent Assistant API Guide</b>
    </div>

    <?php
      error_reporting(null);

      $baseUrl = "http://mts.wibro.agh.edu.pl/~s292511/php/API/";
      $directories = array('Auth', 'Cars', 'Users', 'Leases', 'Documents', 'Tasks', 'Clients');

      foreach ($directories as $dir) {
        echo '<div class="controller"><h1>'.$dir.' Controller</h1>';

        $endpoints = scandir('../'.$dir);
  
        foreach ($endpoints as $item) {
          if (strlen($item) > 5 && substr($item, -4) == '.php') {
            echo '<h2>'.$baseUrl.$dir.'/'.$item.'</h2>';
            echo '<button onclick=\'expand('.json_encode($dir).', '.json_encode($item).')\' class="info-btn" type="button">Expand</button><br/>';
            echo '<span class="endpointDescription" id="endpointDescription'.$dir.$item.'"></span>';
          }
        }
        echo '</div>';
      }

      include_once '../../Models/Enums.php';
      $enums = new Enums();

      echo '<div class = "controller enum"><h2>Enumerators</h2><br>'.$enums->outputJson().'</div>';

    ?>

    <script src="swagger.js"></script>

  </body>
  </html>