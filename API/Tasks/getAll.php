<?php

    // headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: GET');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Access-Control-Allow-Methods, Content-Type, Authorization, X-Requested-With');

    include_once '../../Config/Database.php';
    include_once '../../Models/TaskModel.php';
    include_once '../../Controllers/TaskController.php';

    // headers routing
    foreach (getallheaders() as $name => $value) {
        if ($name == 'swagger' && $value = 'swagger-ngx') {
            $task = new TaskModel();
            echo '"<b>Input: </b> - <br/><b>Output: </b>Array['.$task->outputJson().']</b><br/>"';
            exit();
        }
    }

    // db connection
    $database = new Database();
    $connection = $database->connect();

    $controller = new TaskController($connection);

    // result
    $result = $controller->get();
    $rows = $result->rowCount();

    if ($rows > 0) {
        $users = array();
        while ($user = $result->fetch(PDO::FETCH_ASSOC)) {
            array_push($users, $user);
        }
        echo json_encode($users);
    } else {
        echo json_encode(
            array('message' => 'Unable to fetch data.')
        );
    }

?>