<?php

    // headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: GET');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Access-Control-Allow-Methods, Content-Type, Authorization, X-Requested-With');

    include_once '../../Config/Database.php';
    include_once '../../Models/CarModel.php';
    include_once '../../Controllers/CarController.php';

    // headers routing
    foreach (getallheaders() as $name => $value) {
        if ($name == 'swagger' && $value = 'swagger-ngx') {
            $car = new CarModel();
            echo '"<b>Input: </b> - <br/><b>Output: </b>Array['.$car->outputJson().']</b>"';
            exit();
        }
    }

    // db connection
    $database = new Database();
    $connection = $database->connect();

    $controller = new CarController($connection);

    // result
    $result = $controller->get();
    $rows = $result->rowCount();

    if ($rows > 0) {
        $cars = array();

        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            extract($row);
            $car = array (
                'id' => $id,
                'department' => $department,
                'registrationNumber' => $registrationNumber,
                'brand' => $brand,
                'model' => $model,
                'acriss' => $acriss,
                'tires' => $tires,
                'vin' => $vin,
                'productionYear' => $productionYear,
                'status' => $status,
            );
            array_push($cars, $car);
        }
        echo json_encode($cars);
    } else {
        echo json_encode(
            array('message' => 'Unable to fetch data.')
        );
    }

?>