<?php

    // headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Access-Control-Allow-Methods, Content-Type, Authorization, X-Requested-With');

    include_once '../../Config/Database.php';
    include_once '../../Models/CarModel.php';
    include_once '../../Models/EmptyResponse.php';
    include_once '../../Controllers/CarController.php';

    // headers routing
    foreach (getallheaders() as $name => $value) {
        if ($name == 'swagger' && $value = 'swagger-ngx') {
            $car = new CarModel();
            $out = new EmptyResponse();
            echo '"<b>Input: </b> '.$car->outputJson().'<br/><b>Output: </b>'.$out->outputJson().'</b><br/>"';
            exit();
        }
    }

    // db connection
    $database = new Database();
    $connection = $database->connect();

    // getting payload
    $data = json_decode(file_get_contents("php://input"));

    // response
    $controller = new CarController($connection);

    if ($controller->create($data->department, $data->registrationNumber, $data->brand, $data->model, $data->acriss, $data->tires, $data->vin, $data->productionYear, $data->status)) {
        echo json_encode(
            array('message' => 'Car created.')
        );
    } else {
        echo json_encode(
            array('message' => 'Failed to create the car.')
        );
    }

?>