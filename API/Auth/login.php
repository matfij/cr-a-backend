<?php

    // headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: GET');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Access-Control-Allow-Methods, Content-Type, Authorization, X-Requested-With');

    include_once '../../Config/Database.php';
    include_once '../../Controllers/AuthController.php';
    include_once '../../Models/UserModel.php';

    // headers routing
    foreach (getallheaders() as $name => $value) {
        if ($name == 'swagger' && $value = 'swagger-ngx') {
            $user = new UserModel();
            echo '"<b>Input: </b> {login: string, password: string} <br/><b>Output: </b>'.$user->outputJson().'</b>"';
            exit();
        }
    }
    
    // db connection
    $database = new Database();
    $connection = $database->connect();

    // getting payload
    $data = json_decode(file_get_contents("php://input"));
    $login = $data->login;
    $password = $data->password;

    $controller = new AuthController($connection);

    // result
    $result = $controller->login($login, $password);
    
    if ($result != false) {
        echo json_encode($result);
    } else {
        echo json_encode(
            array('message' => 'Wrong login or password.')
        );
    }

?>