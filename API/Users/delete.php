<?php

    // headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: DELETE');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Access-Control-Allow-Methods, Content-Type, Authorization, X-Requested-With');

    include_once '../../Config/Database.php';
    include_once '../../Models/UserModel.php';
    include_once '../../Models/EmptyResponse.php';
    include_once '../../Controllers/UserController.php';

    // headers routing
    foreach (getallheaders() as $name => $value) {
        if ($name == 'swagger' && $value = 'swagger-ngx') {
            $out = new EmptyResponse();
            echo '"<b>Input: </b>{id: int}<br/><b>Output: </b>'.$out->outputJson().'</b><br/>"';
            exit();
        }
    }

    echo json_encode(
        array('message' => 'Access denied.')
    );
    exit();

    ////////////////////////////////////////////////////////////////////////////////////////

    // db connection
    $database = new Database();
    $connection = $database->connect();

    // getting payload
    $data = json_decode(file_get_contents("php://input"));

    $controller = new UserController($connection);

    // response
    if ($controller->delete($data->id)) {
        echo json_encode(
            array('message' => 'User deleted.')
        );
    } else {
        echo json_encode(
            array('message' => 'Failed to delete the user.')
        );
    }

?>