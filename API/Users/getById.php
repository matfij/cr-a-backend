<?php

    // headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: GET');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Access-Control-Allow-Methods, Content-Type, Authorization, X-Requested-With');

    include_once '../../Config/Database.php';
    include_once '../../Models/UserModel.php';
    include_once '../../Controllers/UserController.php';

    // headers routing
    foreach (getallheaders() as $name => $value) {
        if ($name == 'swagger' && $value = 'swagger-ngx') {
            $user = new UserModel();
            echo '"<b>Input: </b>{id: int}<br/><b>Output: </b>'.$user->outputJson().'</b><br/>"';
            exit();
        }
    }

    // db connection
    $database = new Database();
    $connection = $database->connect();

    // getting payload
    $data = json_decode(file_get_contents("php://input"));
    $id = $data->id;

    $controller = new UserController($connection);

    // result
    $result = $controller->getById($id);
    
    if ($result != null) {
        echo json_encode($result);
    } else {
        echo json_encode(
            array('message' => 'Unable to fetch data.')
        );
    }

?>