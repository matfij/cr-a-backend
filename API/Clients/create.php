<?php

    // headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Access-Control-Allow-Methods, Content-Type, Authorization, X-Requested-With');

    include_once '../../Config/Database.php';
    include_once '../../Models/ClientModel.php';
    include_once '../../Models/EmptyResponse.php';
    include_once '../../Controllers/ClientController.php';

    // headers routing
    foreach (getallheaders() as $name => $value) {
        if ($name == 'swagger' && $value = 'swagger-ngx') {
            $client = new ClientModel();
            $out = new EmptyResponse();
            echo '"<b>Input: </b>'.$client->outputJson().'<br/><b>Output: </b>'.$out->outputJson().'</b><br/>"';
            exit();
        }
    }

    // db connection
    $database = new Database();
    $connection = $database->connect();

    // getting payload
    $data = json_decode(file_get_contents("php://input"));

    $controller = new ClientController($connection);

    // response
    if ($controller->create($data->name, $data->surname, $data->age, $data->pesel, $data->address, $data->email, $data->phone)) {
        echo json_encode(
            array('message' => 'Client created.')
        );
    } else {
        echo json_encode(
            array('message' => 'Failed to create the client.')
        );
    }

?>