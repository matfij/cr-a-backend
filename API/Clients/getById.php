<?php

    // headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: GET');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Access-Control-Allow-Methods, Content-Type, Authorization, X-Requested-With');

    include_once '../../Config/Database.php';
    include_once '../../Models/ClientModel.php';
    include_once '../../Controllers/ClientController.php';

    // headers routing
    foreach (getallheaders() as $name => $value) {
        if ($name == 'swagger' && $value = 'swagger-ngx') {
            $client = new ClientModel();
            echo '"<b>Input: </b>{id: int}<br/><b>Output: </b>'.$client->outputJson().'</b><br/>"';
            exit();
        }
    }

    // db connection
    $database = new Database();
    $connection = $database->connect();

    // getting payload
    $data = json_decode(file_get_contents("php://input"));
    $id = $data->id;

    $controller = new ClientController($connection);

    // result
    $result = $controller->getById($id);
    
    if ($result != null) {
        echo json_encode($result);
    } else {
        echo json_encode(
            array('message' => 'Unable to fetch data.')
        );
    }

?>