<?php

    // headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST, PUT');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Access-Control-Allow-Methods, Content-Type, Authorization, X-Requested-With');

    include_once '../../Config/Database.php';
    include_once '../../Models/LeaseModel.php';
    include_once '../../Models/EmptyResponse.php';
    include_once '../../Controllers/LeaseController.php';

    // headers routing
    foreach (getallheaders() as $name => $value) {
        if ($name == 'swagger' && $value = 'swagger-ngx') {
            $lease = new LeaseModel();
            $out = new EmptyResponse();
            echo '"<b>Input: </b>'.$lease->outputJson().'<br/><b>Output: </b>'.$out->outputJson().'</b><br/>"';
            exit();
        }
    }

    // db connection
    $database = new Database();
    $connection = $database->connect();

    // getting payload
    $data = json_decode(file_get_contents("php://input"));

    $controller = new LeaseController($connection);

    // response
    if ($controller->update($data->id, $data->clientId, $data->carId, $data->startDate, $data->endDate, $data->status, $data->department)) {
        echo json_encode(
            array('message' => 'Lease updated.')
        );
    } else {
        echo json_encode(
            array('message' => 'Failed to update the lease.')
        );
    }

?>