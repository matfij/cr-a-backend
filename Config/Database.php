<?php

    class Database {

        // db params
        private $host = 'localhost';
        private $db_name = 's292511';
        private $username = 's292511';
        private $password = 'jamroggrzegorz';
        private $conn;
    
        // db connection method
        public function connect() {
            $this->conn = null;

            try {
                $this->conn = new PDO('mysql:host=' . $this->host . ';dbname=' . $this->db_name, $this->username, $this->password);
                $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            catch (PDOException $e) {
                echo 'Failed to connect to database:' . $e->getMessage();
            }

            return $this->conn;
        }
    }

?>