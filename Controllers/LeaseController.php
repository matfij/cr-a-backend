<?php

    class LeaseController {

        private $conn;
        private $table = 'Leases';

        public function __construct($db) {
            $this->conn = $db;
        }

        // get all leases
        public function get() {
            $query = 'SELECT * FROM '.$this->table;

            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            return $stmt;
        }

        // get lease by id
        public function getById($id) {
            $id = htmlspecialchars(strip_tags($id));

            $query = 'SELECT * FROM '.$this->table.' WHERE Id = ?';

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(1, $id);
            $stmt->execute();

            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;
        }

        // create a new lease
        public function create($clientId, $carId, $startDate, $endDate, $status, $department) {
            $clientId = htmlspecialchars(strip_tags($clientId));
            $carId = htmlspecialchars(strip_tags($carId));
            $startDate = htmlspecialchars(strip_tags($startDate));
            $endDate = htmlspecialchars(strip_tags($endDate));
            $status = htmlspecialchars(strip_tags($status));
            $department = htmlspecialchars(strip_tags($department));

            $query = 'INSERT INTO '.$this->table.' 
                (clientId, carId, startDate, endDate, status, department) 
                VALUES ('.$clientId.', '.$carId.', "'.$startDate.'", "'.$endDate.'", '.$status.', "'.$department.'")';

            $stmt = $this->conn->prepare($query);
            
            if ($stmt->execute())  return $stmt;
            else  return false;
        }

        // update an existing lease
        public function update($id, $clientId, $carId, $startDate, $endDate, $status, $department) {
            $id = htmlspecialchars(strip_tags($id));
            $clientId = htmlspecialchars(strip_tags($clientId));
            $carId = htmlspecialchars(strip_tags($carId));
            $startDate = htmlspecialchars(strip_tags($startDate));
            $endDate = htmlspecialchars(strip_tags($endDate));
            $status = htmlspecialchars(strip_tags($status));
            $department = htmlspecialchars(strip_tags($department));

            $query = 'UPDATE '.$this->table.' 
                SET clientId = '.$clientId.', carId = '.$carId.', startDate = "'.$startDate.'", endDate = "'.$endDate.'", status = '.$status.', department = "'.$department .'"
                WHERE id='.$id;

            $stmt = $this->conn->prepare($query);
            
            if ($stmt->execute())  return true;
            else  return false;
        }
    }

?>