<?php

    class UserController {

        private $conn;
        private $table = 'Users';

        public function __construct($db) {
            $this->conn = $db;
        }

        // get all users
        public function get() {
            $query = 'SELECT * FROM '.$this->table;

            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            return $stmt;
        }

        // get user by id
        public function getById($id) {
            $id = htmlspecialchars(strip_tags($id));

            $query = 'SELECT * FROM '.$this->table.' WHERE Id = ?';

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(1, $id);
            $stmt->execute();

            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;
        }

        // create a new user
        public function create($name, $login, $password, $email, $role, $department) {
            $name = htmlspecialchars(strip_tags($name));
            $login = htmlspecialchars(strip_tags($login));
            $password = htmlspecialchars(strip_tags($password));
            $email = htmlspecialchars(strip_tags($email));
            $role = htmlspecialchars(strip_tags($role));
            $department = htmlspecialchars(strip_tags($department));

            $query = 'INSERT INTO '.$this->table.' 
                (Name, Login, Password, Email, Role, Department) 
                VALUES ("'.$name.'", "'.$login.'","'.$password.'","'.$email.'", '.$role.', "'.$department.'")';

            $stmt = $this->conn->prepare($query);
            
            if ($stmt->execute())  return $stmt;
            else  return false;
        }

        // update an existing user
        public function update($id, $name, $login, $password, $email, $role, $department) {
            $id = htmlspecialchars(strip_tags($id));
            $name = htmlspecialchars(strip_tags($name));
            $login = htmlspecialchars(strip_tags($login));
            $password = htmlspecialchars(strip_tags($password));
            $email = htmlspecialchars(strip_tags($email));
            $role = htmlspecialchars(strip_tags($role));
            $department = htmlspecialchars(strip_tags($department));

            $query = 'UPDATE '.$this->table.' 
                SET Name="'.$name.'", Login="'.$login.'", Password="'.$password.'", Email="'.$email.'", Role='.$role.', Department="'.$department.'" 
                WHERE Id='.$id;

            $stmt = $this->conn->prepare($query);
            
            if ($stmt->execute())  return true;
            else  return false;
        }

        // delete an existing user
        public function delete($id) {
            $id = htmlspecialchars(strip_tags($id));

            $query = 'DELETE FROM '.$this->table.' WHERE Id = '.$id;

            $stmt = $this->conn->prepare($query);
            
            if ($stmt->execute())  return true;
            else  return false;
        }
    }

?>