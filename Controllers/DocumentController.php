<?php

    include_once 'LeaseController.php';
    include_once 'ClientController.php';
    include_once 'CarController.php';
    include_once 'TaskController.php';

    class DocumentController {

        private $conn;
        private $table = 'Documents';

        public function __construct($db) {
            $this->conn = $db;
        }

        // get documents info
        public function getInfo() {
            $query = 'SELECT id, leaseId, type FROM '.$this->table;

            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            return $stmt;
        }

        // get document by id
        public function getById($id) {
            $id = htmlspecialchars(strip_tags($id));

            $query = 'SELECT * FROM '.$this->table.' WHERE Id = ?';

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(1, $id);
            $stmt->execute();

            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;
        }

        // store document
        public function store($leaseId, $type, $file) {
            $leaseId = htmlspecialchars(strip_tags($leaseId));
            $type = htmlspecialchars(strip_tags($type));

            $pdf = $file->Output('', 'S');
            $pdf = addslashes($pdf);

            $query = 'INSERT INTO '.$this->table.' 
                (leaseId, type, file) 
                VALUES ('.$leaseId.', '.$type.', "'.$pdf.'")';

            $stmt = $this->conn->prepare($query);
            
            if ($stmt->execute())  return $stmt;
            else  return false; 
        }

        // generate let out protocol
        public function generateLetOutProtocol($leaseId, $comments) {
            $leaseId = htmlspecialchars(strip_tags($leaseId));
            $comments = htmlspecialchars(strip_tags($comments));

            // get lease data
            $controller = new LeaseController($this->conn);
            $lease = $controller->getById($leaseId);

            // get task data
            $controller = new TaskController($this->conn);
            $task = $controller->getById($leaseId);

            // get client data
            $controller = new ClientController($this->conn);
            $client = $controller->getById($lease["clientId"]);

            // get car data
            $controller = new CarController($this->conn);
            $car = $controller->getById($lease["carId"]);

            $html = '
                <h1>LET OUT PROTOCOL</h1>
                <h2>GENERAL TERMS OF LEASE AGREEMENT</h2>
                <ol style="text-align: justify;">
                    <li>
                        Curabitur venenatis est id mi tincidunt, eu cursus quam pretium. Etiam tristique elit eget accumsan posuere. Aliquam facilisis urna ligula, quis condimentum elit laoreet at. Donec arcu velit, aliquet eu facilisis laoreet, faucibus et nibh. Donec luctus erat nec justo ultricies hendrerit. Sed sollicitudin auctor leo at mattis.
                    </li>
                    <li>
                        Nulla pulvinar lectus et sapien sollicitudin, sed vehicula lectus tincidunt. Integer dignissim finibus elit sed auctor. Vestibulum rhoncus ex ut quam pellentesque accumsan. Suspendisse suscipit tincidunt pretium. Maecenas vitae sollicitudin purus. Mauris hendrerit ipsum arcu, sit amet maximus sem lobortis et.
                    </li>
                </ol>
                <hr>
                <h2>LEASE</h2>
                <h3>Start Date: '.$lease["startDate"].' &nbsp; Address: '.$task["address"].'</h3>
                <hr>
                <h2>USER</h2>
                <h3>First name: '.$client["name"].' &nbsp; Last name: '.$client["surname"].'</h3>
                <h3>Pesel: '.$client["pesel"].' &nbsp; Address: '.$client["address"].'</h3>
                <hr>
                <h2>CAR</h2>
                <h3>Registration number: '.$car["registrationNumber"].'</h3>
                <h3>Brand: '.$car["brand"].' &nbsp; Model: '.$car["model"].'</h3>
                <h3>Mileage: '.$car["mileage"].' &nbsp; <span style="color: red;"> Purity: STERILIZED</span> </h3>
                <hr>
                <h2>Comments: </h2>
                <h3>'.$comments.'</h3>
                <hr>
                <h2>Client sign:</h2>
            ';
            
            return $html;
        }

        public function generateTakeBackProtocol($leaseId, $comments) {
            $leaseId = htmlspecialchars(strip_tags($leaseId));
            $comments = htmlspecialchars(strip_tags($comments));

            // get lease data
            $controller = new LeaseController($this->conn);
            $lease = $controller->getById($leaseId);

            // get task data
            $controller = new TaskController($this->conn);
            $task = $controller->getById($leaseId);

            // get client data
            $controller = new ClientController($this->conn);
            $client = $controller->getById($lease["clientId"]);

            // get car data
            $controller = new CarController($this->conn);
            $car = $controller->getById($lease["carId"]);
            $html = '
                <h1>TAKE BACK PROTOCOL</h1>
                    <h2>LEASE ANNEX</h2>
                    <span style="text-align: justify;">
                    Curabitur venenatis est id mi tincidunt, eu cursus quam pretium. Etiam tristique elit eget accumsan posuere. Aliquam facilisis urna ligula, quis condimentum elit laoreet at. Donec arcu velit, aliquet eu facilisis laoreet, faucibus et nibh. Donec luctus erat nec justo ultricies hendrerit. Sed sollicitudin auctor leo at mattis.
                    </span>
                    <hr>
                    <h2>LEASE</h2>
                    <h3>Finish Date: '.$lease["endDate"].' &nbsp; Address: '.$task["address"].'</h3>
                    <hr>
                    <h2>USER</h2>
                    <h3>First name: '.$client["name"].' &nbsp; Last name: '.$client["surname"].'</h3>
                    <h3>Pesel: '.$client["pesel"].' &nbsp; Address: '.$client["address"].'</h3>
                    <hr>
                    <h2>CAR</h2>
                    <h3>Registration number: '.$car["registrationNumber"].'</h3>
                    <h3>Brand: '.$car["brand"].' &nbsp; Model: '.$car["model"].'</h3>
                    <h3>Mileage: '.$car["mileage"].' &nbsp; <span style="color: red;"> Purity: STERILIZED</span> </h3>
                    <hr>
                    <h2>Comments: </h2>
                    <h3>'.$comments.'</h3>
                    <hr>
                    <h2>Client sign:</h2>
                ';
            
            return $html;
        }

    }

?>