<?php

    class CarController {

        private $conn;
        private $table = 'Cars';

        public function __construct($db) {
            $this->conn = $db;
        }

        // get all cars
        public function get() {
            $query = 'SELECT * FROM '.$this->table;

            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            return $stmt;
        }

        // get car by id
        public function getById($id) {
            $id = htmlspecialchars(strip_tags($id));

            $query = 'SELECT * FROM '.$this->table.' WHERE Id = ?';

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(1, $id);
            $stmt->execute();

            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;
        }

        // create a new car
        public function create($department, $registrationNumber, $brand, $model, $acriss, $tires, $vin, $productionYear, $status) {
            $department = htmlspecialchars(strip_tags($department));
            $registrationNumber = htmlspecialchars(strip_tags($registrationNumber));
            $brand = htmlspecialchars(strip_tags($brand));
            $model = htmlspecialchars(strip_tags($model));
            $acriss = htmlspecialchars(strip_tags($acriss));
            $tires = htmlspecialchars(strip_tags($tires));
            $vin = htmlspecialchars(strip_tags($vin));
            $productionYear = htmlspecialchars(strip_tags($productionYear));
            $status = htmlspecialchars(strip_tags($status));

            $query = 'INSERT INTO '.$this->table.' 
                (Department, RegistrationNumber, Brand, Model, Acriss, Tires, Vin, ProductionYear, Status) 
                VALUES ("'.$department.'", "'.$registrationNumber.'", "'.$brand.'", "'.$model.'", "'.$acriss.'", "'.$tires.'", "'.$vin.'", '.$productionYear.', '.$status.')';

            $stmt = $this->conn->prepare($query);
            
            if ($stmt->execute())  return $stmt;
            else  return false;
        }

        // update an existing car
        public function update($id, $department, $registrationNumber, $brand, $model, $acriss, $tires, $vin, $productionYear, $status) {
            $id = htmlspecialchars(strip_tags($id));
            $department = htmlspecialchars(strip_tags($department));
            $registrationNumber = htmlspecialchars(strip_tags($registrationNumber));
            $brand = htmlspecialchars(strip_tags($brand));
            $model = htmlspecialchars(strip_tags($model));
            $acriss = htmlspecialchars(strip_tags($acriss));
            $tires = htmlspecialchars(strip_tags($tires));
            $vin = htmlspecialchars(strip_tags($vin));
            $productionYear = htmlspecialchars(strip_tags($productionYear));
            $status = htmlspecialchars(strip_tags($status));

            $query = 'UPDATE '.$this->table.' 
                SET Department="'.$department.'", RegistrationNumber="'.$registrationNumber.'", Brand="'.$brand.'", Model="'.$model.'", Acriss="'.$acriss.'", Tires="'.$tires.'", Vin="'.$vin.'", ProductionYear='.$productionYear.', Status='.$status.'  
                WHERE Id='.$id;

            $stmt = $this->conn->prepare($query);
            
            if ($stmt->execute())  return true;
            else  return false;
        }

        // delete an existing car
        public function delete($id) {
            $id = htmlspecialchars(strip_tags($id));

            $query = 'DELETE FROM '.$this->table.' WHERE Id = '.$id;

            $stmt = $this->conn->prepare($query);
            
            if ($stmt->execute())  return true;
            else  return false;
        }
    }

?>