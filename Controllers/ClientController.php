<?php

    class ClientController {

        private $conn;
        private $table = 'Clients';

        public function __construct($db) {
            $this->conn = $db;
        }

        // get all clients
        public function get() {
            $query = 'SELECT * FROM '.$this->table;

            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            return $stmt;
        }

        // get client by id
        public function getById($id) {
            $id = htmlspecialchars(strip_tags($id));

            $query = 'SELECT * FROM '.$this->table.' WHERE Id = ?';

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(1, $id);
            $stmt->execute();

            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;
        }

        // create a new client
        public function create($name, $surname, $age, $pesel, $address, $email, $phone) {
            $name = htmlspecialchars(strip_tags($name));
            $surname = htmlspecialchars(strip_tags($surname));
            $age = htmlspecialchars(strip_tags($age));
            $pesel = htmlspecialchars(strip_tags($pesel));
            $address = htmlspecialchars(strip_tags($address));
            $email = htmlspecialchars(strip_tags($email));
            $phone = htmlspecialchars(strip_tags($phone));

            $query = 'INSERT INTO '.$this->table.' 
                (name, surname, age, pesel, address, email, phone) 
                VALUES ("'.$name.'", "'.$surname.'", '.$age.', "'.$pesel.'", "'.$address.'", "'.$email.'", '.$phone.')';

            $stmt = $this->conn->prepare($query);
            
            if ($stmt->execute())  return $stmt;
            else  return false;
        }

        // update an existing client
        public function update($id, $name, $surname, $age, $pesel, $address, $email, $phone) {
            $id = htmlspecialchars(strip_tags($id));
            $name = htmlspecialchars(strip_tags($name));
            $surname = htmlspecialchars(strip_tags($surname));
            $age = htmlspecialchars(strip_tags($age));
            $pesel = htmlspecialchars(strip_tags($pesel));
            $address = htmlspecialchars(strip_tags($address));
            $email = htmlspecialchars(strip_tags($email));
            $phone = htmlspecialchars(strip_tags($phone));

            $query = 'UPDATE '.$this->table.' 
                SET name="'.$name.'", surname="'.$surname.'", age='.$age.', pesel="'.$pesel.'", address="'.$address.'", email="'.$email.'", phone='.$phone.' 
                WHERE Id='.$id;

            $stmt = $this->conn->prepare($query);
            
            if ($stmt->execute())  return true;
            else  return false;
        }

        // delete an existing user
        public function delete($id) {
            $id = htmlspecialchars(strip_tags($id));

            $query = 'DELETE FROM '.$this->table.' WHERE Id = '.$id;

            $stmt = $this->conn->prepare($query);
            
            if ($stmt->execute())  return true;
            else  return false;
        }
    }

?>