<?php

    class TaskController {

        private $conn;
        private $table = 'Tasks';

        public function __construct($db) {
            $this->conn = $db;
        }

        // get all leases
        public function get() {
            $query = 'SELECT * FROM '.$this->table;

            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            return $stmt;
        }

        // get lease by id
        public function getById($id) {
            $id = htmlspecialchars(strip_tags($id));

            $query = 'SELECT * FROM '.$this->table.' WHERE Id = ?';

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(1, $id);
            $stmt->execute();

            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;
        }

        // create a new task
        public function create($leaseId, $userId, $type, $status, $address) {
            $leaseId = htmlspecialchars(strip_tags($leaseId));
            $userId = htmlspecialchars(strip_tags($userId));
            $type = htmlspecialchars(strip_tags($type));
            $status = htmlspecialchars(strip_tags($status));
            $address = htmlspecialchars(strip_tags($address));

            $query = 'INSERT INTO '.$this->table.' 
                (leaseId, userId, type, status, address) 
                VALUES ('.$leaseId.', '.$userId.', '.$type.',  '.$status.', "'.$address.'")';

            $stmt = $this->conn->prepare($query);
            
            if ($stmt->execute())  return $stmt;
            else  return false;
        }

        // update an existing task
        public function update($id, $leaseId, $userId, $type, $status, $address) {
            $id = htmlspecialchars(strip_tags($id));
            $leaseId = htmlspecialchars(strip_tags($leaseId));
            $userId = htmlspecialchars(strip_tags($userId));
            $type = htmlspecialchars(strip_tags($type));
            $status = htmlspecialchars(strip_tags($status));
            $address = htmlspecialchars(strip_tags($address));

            $query = 'UPDATE '.$this->table.' 
                SET leaseId = '.$leaseId.', userId = '.$userId.', type = '.$type.', status = '.$status.', address = "'.$address .'"
                WHERE id='.$id;

            $stmt = $this->conn->prepare($query);
            
            if ($stmt->execute())  return true;
            else  return false;
        }

        public function getByLease($leaseId) {
            $leaseId = htmlspecialchars(strip_tags($leaseId));

            $query = 'SELECT * FROM '.$this->table.' WHERE leaseId='.$leaseId;

            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;
        }
    }

?>